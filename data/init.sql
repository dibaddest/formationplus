CREATE IF NOT EXISTS DATABASE db_formationplus;
USE db_formationplus;

CREATE IF NOT EXISTS TABLE convention (
  id VARCHAR(20),
  nb_heur VARCHAR(10),
  nom VARCHAR(25)
);

CREATE IF NOT EXISTS TABLE convention (
  id BIGINT,
  nb_heur INT,
  nom VARCHAR(255)
);

CREATE IF NOT EXISTS TABLE etudiant (
  id BIGINT,
  mail VARCHAR(255),
  nom VARCHAR(255),
  prenom VARCHAR(255),
  convention_id BIGINT
);


insert into db_formationplus.convention (id, nb_heur, nom)
values  (1, 400, 'CDA1'),
        (2, 250, 'AL1'),
        (3, 400, 'CDA2'),
        (4, 250, 'AL2'),
        (5, 400, 'CDA3'),
        (6, 250, 'AL3'),
        (7, 400, 'CDA4'),
        (8, 250, 'AL4'),
        (9, 400, 'CDA5'),
        (10, 250, 'AL5');

insert into db_formationplus.etudiant (id, mail, nom, prenom, convention_id)
values  (11, 'mateusz.tirel@gmail.com', 'Tirel', 'Mateusz', 1),
        (12, 'bruno.provenier@gmail.com', 'Provenier', 'Bruno', 1),
        (13, 'antoinemotte@gmail.com', 'Motte', 'Antoine', 2),
        (14, 'kammegne2002@yahoo.fr', 'Kammegne', 'Paul', 2),
        (15, 'nelly_ond@yahoo.fr', 'Ond', 'Nelly', 3),
        (16, 'ysenic@gmail.com', 'Senicourt', 'Yann', 3),
        (17, 'ibrahim.sacopro@gmail.com', 'Saco', 'Ibrahim', 4),
        (18, 'os.krid@gmail.com', 'Krid', 'Othmann', 4),
        (19, 'hfaouki@gmail.com', 'Faouki', 'Hasnaa', 5),
        (20, 'fara2raz@yahoo.com', 'Raz', 'Fara', 5),
        (21, 'billal.benziane1@gmail.com', 'Benziane', 'Billal', 6),
        (22, 'marwa.chguar@gmail.com', 'Chguar', 'Marwa', 6),
        (23, 'fkanfana@yahoo.fr', 'Kanfana', 'Fatoumata', 7),
        (24, 'romykombet@gmail.com', 'Kombet', 'Romy', 7),
        (26, 'antoine.blk@gmail.com', 'Boulkeroua', 'Antoine', 8),
        (27, 'parmenon.damien@gmail.com', 'Parmenon', 'Damien', 8),
        (28, 'gkastane@gmail.com', 'Guytri', 'Kastane', 9);
