package com.formationplus.api.entity;

import javax.persistence.*;

@Entity
public class Attestation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn()
    private Etudiant etudiant;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn()
    private Convention convention;

    @Lob
    private String message;

    public Attestation() {
    }

    public Attestation(Etudiant etudiant, Convention convention, String message) {
        this.etudiant = etudiant;
        this.convention = convention;
        this.message = message;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Etudiant getEtudiant() {
        return etudiant;
    }

    public void setEtudiant(Etudiant etudiant) {
        this.etudiant = etudiant;
    }

    public Convention getConvention() {
        return convention;
    }

    public void setConvention(Convention convention) {
        this.convention = convention;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
