package com.formationplus.api.controller;

import com.formationplus.api.entity.Etudiant;
import com.formationplus.api.service.EtudiantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Api(description = "API pour les opérations CRUD concernant les étudiants")
@RestController
@RequestMapping("/formation-plus/api/Etudiant")
public class EtudiantController {
    @Autowired
    private EtudiantService service;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ApiOperation(value = "Création et persistene d'un obket Attestation")
    @PostMapping(path = "/create")
    public ResponseEntity<Etudiant> createEtudiant(@RequestBody Etudiant etudiant) {
        Etudiant etudiantToCreate = service.saveEtudiant(etudiant);
        return new ResponseEntity<>(etudiantToCreate, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Recherche un étudiant à partir de ID")
    @GetMapping(path = "/{id}")
    public Etudiant getEtudiant(@PathVariable Long id) {
        return service.findEtudiant(id);
    }

    @ApiOperation(value = "Lister tous les étudiants à partir de ID")
    @GetMapping(path = "/")
    public List<Etudiant> getAllEtudiants() {
        return service.getAllEtudiants();
    }


}
