package com.formationplus.api.controller;

public class AttestationPOJO {
    private Long id;

    private Long etudiantId;

    private Long conventionId;

    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEtudiantId() {
        return etudiantId;
    }

    public void setEtudiantId(Long etudiantId) {
        this.etudiantId = etudiantId;
    }

    public Long getConventionId() {
        return conventionId;
    }

    public void setConventionId(Long conventionId) {
        this.conventionId = conventionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
