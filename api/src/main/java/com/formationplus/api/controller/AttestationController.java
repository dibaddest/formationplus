package com.formationplus.api.controller;

import com.formationplus.api.entity.Attestation;
import com.formationplus.api.entity.Convention;
import com.formationplus.api.service.AttestationService;
import com.formationplus.api.service.ConventionService;
import com.formationplus.api.service.EtudiantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Api(description = "API pour les opérations CRUD concernant les attestations")
@RestController
@RequestMapping(path = "/formation-plus/api/Attestation")
public class AttestationController {
    @Autowired
    private AttestationService service;

    @Autowired
    private EtudiantService etudiantService;

    @Autowired
    private ConventionService conventionService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ApiOperation(value = "Recherche une attestation à partir de ID")
    @GetMapping(path = "/{id}")
    public Attestation findAttestationById(@PathVariable Long id) {
        return null;
    }

    @ApiOperation(value = "Création et persistene d'un obket Attestation")
    @PostMapping(path = "create")
    public ResponseEntity<Attestation> createAttestation(@RequestBody AttestationPOJO attestationPOJO) {
        Attestation attestation = new Attestation(
            etudiantService.findEtudiant(attestationPOJO.getEtudiantId()),
            conventionService.findConvention(attestationPOJO.getConventionId()),
            attestationPOJO.getMessage()
        );
        Attestation attestationToCreate = service.saveAttestation(attestation);
        return new ResponseEntity<>(attestationToCreate, HttpStatus.CREATED);
    }

}
