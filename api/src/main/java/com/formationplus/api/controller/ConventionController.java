package com.formationplus.api.controller;

import com.formationplus.api.entity.Convention;
import com.formationplus.api.service.ConventionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description = "API pour les opérations CRUD concernant les convnetions")
@RestController
@RequestMapping("/formation-plus/api/Convention")
public class ConventionController {

    @Autowired
    private ConventionService service;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ApiOperation(value = "Création et persistene d'un obket Attestation")
    @PostMapping(path = "/create")
    public ResponseEntity<Convention> createConvention(@RequestBody Convention convention) {
        Convention conventionToCreate = service.saveConvention(convention);
        return new ResponseEntity<>(conventionToCreate, HttpStatus.CREATED);
    }

    @ApiOperation(value = "Recherche un étudiant à partir de ID")
    @GetMapping("/{id}")
    public Convention getConvention(@PathVariable Long id) {
        return service.findConvention(id);
    }

    @ApiOperation(value = "Lister tous les étudiants à partir de ID")
    @GetMapping(path = "/conventions")
    public List<Convention> getAllConventions() {
        return service.getAllConvention();
    }

}
