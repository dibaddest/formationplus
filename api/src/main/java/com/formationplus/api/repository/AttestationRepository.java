package com.formationplus.api.repository;


import com.formationplus.api.entity.Attestation;
import org.springframework.data.repository.CrudRepository;

public interface AttestationRepository extends CrudRepository<Attestation, Long> {

}
