package com.formationplus.api.repository;

import com.formationplus.api.entity.Etudiant;
import org.springframework.data.repository.CrudRepository;

public interface EtudiantRepository extends CrudRepository<Etudiant, Long> {

}
