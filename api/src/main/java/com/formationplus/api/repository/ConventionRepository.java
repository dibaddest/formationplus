package com.formationplus.api.repository;


import com.formationplus.api.entity.Convention;
import org.springframework.data.repository.CrudRepository;

public interface ConventionRepository extends CrudRepository<Convention, Long> {

}
