package com.formationplus.api.service;



import com.formationplus.api.entity.Convention;

import java.util.List;

public interface ConventionService {
    Convention saveConvention(Convention convention);

    Convention findConvention(Long id);

    List<Convention> getAllConvention();
}
