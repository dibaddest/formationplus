package com.formationplus.api.service;



import com.formationplus.api.entity.Etudiant;

import java.util.List;

public interface EtudiantService {
    Etudiant saveEtudiant(Etudiant etudiant);

    Etudiant findEtudiant(Long id);

    List<Etudiant> getAllEtudiants();
}
