package com.formationplus.api.service;

import com.formationplus.api.entity.Etudiant;
import com.formationplus.api.repository.EtudiantRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EtudiantServiceImpl implements EtudiantService{

    @Autowired
    private EtudiantRepository repository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * @param etudiant 
     * @return
     */
    @Override
    public Etudiant saveEtudiant(Etudiant etudiant) {
        try {
            repository.save(etudiant);
            return etudiant;
        } catch (Exception e) {
            logger.error("saveEtudiant() failed: " + e);
            return null;
        }
    }

    /**
     * @param id 
     * @return
     */
    @Override
    public Etudiant findEtudiant(Long id) {
        try {
            Optional<Etudiant> etudiant = repository.findById(id);
            return etudiant.orElse(null);
        } catch (Exception e) {
            logger.error("L'etudiant n'existe pas : " + e);
            return null;
        }
    }

    /**
     * @return 
     */
    @Override
    public List<Etudiant> getAllEtudiants() {
        try {
            return (List<Etudiant>) repository.findAll();
        } catch (Exception e) {
            logger.error("Erreur dans le listing des Etudiants: " + e);
            return null;
        }

    }
}
