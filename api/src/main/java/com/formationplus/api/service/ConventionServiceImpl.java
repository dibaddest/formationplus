package com.formationplus.api.service;

import com.formationplus.api.entity.Convention;
import com.formationplus.api.repository.ConventionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ConventionServiceImpl implements ConventionService {

    @Autowired
    private ConventionRepository repository;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * @param convention 
     * @return
     */
    @Override
    public Convention saveConvention(Convention convention) {
        try {
            repository.save(convention);
            return convention;
        } catch (Exception e) {
            logger.error("saveConvention() failed : " + e);
            return null;
        }

    }

    /**
     * @param id 
     * @return
     */
    @Override
    public Convention findConvention(Long id) {
        try {
            Optional<Convention> convention = repository.findById(id);
            return convention.orElse(null);
        } catch (Exception e) {
            logger.error("Convention n'existe pas : " + e);
            return null;
        }
    }

    /**
     * @return 
     */
    @Override
    public List<Convention> getAllConvention() {
        try {
            return (List<Convention>) repository.findAll();
        } catch (Exception e) {
            logger.error("Erreur dans le listings des Conventions : " + e);
            return null;
        }
    }
}
