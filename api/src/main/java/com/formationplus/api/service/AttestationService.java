package com.formationplus.api.service;

import com.formationplus.api.entity.Attestation;

public interface AttestationService {
    Attestation saveAttestation(Attestation attestation);
}
