package com.formationplus.api.service;

import com.formationplus.api.entity.Attestation;
import com.formationplus.api.repository.AttestationRepository;
import com.formationplus.api.repository.ConventionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AttestationServiceImpl implements AttestationService {

    @Autowired
    private AttestationRepository repository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Attestation saveAttestation(Attestation attestation) {
        try {
            logger.info(attestation.getEtudiant().getNom());
            logger.info(attestation.getConvention().getNom());
            repository.save(attestation);
            return attestation;
        } catch (Exception e) {
            logger.error("saveAttestation() failed: " + e);
            return null;
        }
    }
}
