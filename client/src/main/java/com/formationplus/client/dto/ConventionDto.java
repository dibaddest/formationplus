package com.formationplus.client.dto;

public class ConventionDto {

    private  Long id;

    private String nom;

    private int nbHeur;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNbHeur() {
        return nbHeur;
    }

    public void setNbHeur(int nbHeur) {
        this.nbHeur = nbHeur;
    }
}
