package com.formationplus.client.proxy;

import com.formationplus.client.dto.AttestationDto;
import com.formationplus.client.dto.ConventionDto;
import com.formationplus.client.dto.EtudiantDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@FeignClient(name = "formationplus-api", url = "localhost:9000")
public interface ApiProxy {
    @GetMapping(path = "/formation-plus/api/Etudiant/")
    List<EtudiantDto> getAllEtudiants();
    @GetMapping(path = "/formation-plus/api/Etudiant/{id}")
    EtudiantDto getEtudiantById(@PathVariable Long id);

    @GetMapping("/formation-plus/api/Convention/{id}")
    ConventionDto getConventionById(@PathVariable Long id);

    @PostMapping(path = "/formation-plus/api/Attestation/create")
    ResponseEntity<AttestationDto> createAttestation(@RequestBody AttestationDto attestation);



}
