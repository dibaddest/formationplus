package com.formationplus.client.controller;

import com.formationplus.client.dto.AttestationDto;
import com.formationplus.client.dto.ConventionDto;
import com.formationplus.client.dto.EtudiantDto;
import com.formationplus.client.proxy.ApiProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ApiProxy apiProxy;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/")
    public String accueil(Model model) {
        List<EtudiantDto> etudiantDtoList = apiProxy.getAllEtudiants();
        model.addAttribute("etudiants", etudiantDtoList);
        model.addAttribute("etudiant", new EtudiantDto());
        return "index";
    }

    @PostMapping("/")
    public String gen(EtudiantDto etudiant, Model model) {
        logger.info("" + etudiant.getId());
        EtudiantDto etudiantDto = apiProxy.getEtudiantById(etudiant.getId());
        AttestationDto attestation = new AttestationDto();
        //logger.info("EtudiantId : " + etudiant.getId());
        String message = "Bonjour " + etudiantDto.getNom() + " " + etudiantDto.getPrenom() +",\n\n" +
                "Vous avez suivi "+ etudiantDto.getConvention().getNbHeur() + " heures de formation chez FormationPlus.\n" +
                "Pouvez-vous nous retourner ce mail avec la pièce jointe signée.\n\n\n" +
                "Cordialement,\n" +
                "FormationPlus";
        ConventionDto convention = apiProxy.getConventionById(etudiantDto.getConvention().getId());
        //logger.info("ConventionId" + convention.getId());
        attestation.setEtudiantId(etudiantDto.getId());
        attestation.setConventionId(convention.getId());
        attestation.setMessage(message);
        model.addAttribute("convention", convention);
        model.addAttribute("attestation", attestation);
        model.addAttribute("etudiant", etudiantDto);
        return "formulaire";
    }

    @PostMapping(path = "/validation")
    public String validation(AttestationDto attestation, Model model) {
        ResponseEntity<AttestationDto> attestationToSave = apiProxy.createAttestation(attestation);
        return "confirmation";
    }
}
