package com.formationplus.client.controller;

import com.formationplus.client.dto.EtudiantDto;
import com.formationplus.client.proxy.ApiProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("etudiants")
public class EtudiantsRestController {

    @Autowired
    public ApiProxy apiProxy;
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping
    public List<EtudiantDto> etudiants(@RequestParam(value = "q", required = false) String query) {
        logger.info(apiProxy.getAllEtudiants().toString());
        if (StringUtils.isEmpty(query)) {
            return apiProxy.getAllEtudiants()
                    .stream()
                    .limit(5)
                    .collect(Collectors.toList());
        }
        return apiProxy.getAllEtudiants()
                .stream()
                .filter( etudiant -> etudiant.getNom().toLowerCase().contains(query))
                .limit(5)
                .collect(Collectors.toList());
    }
}
