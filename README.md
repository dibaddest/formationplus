# FormationPlus
Ce projet est un POC.

Il s'agira de faire un formulaire qui a pour objectif d'insérer une attestation en base de données.

## Lancement de l'API
Le projet est basé sur l'utilisation d'une API. Pour la lancer:
```bash
cd api/
mvn clean install
java -jar target/api-0.0.1-SNAPSHOT.jar
```
Elle est lancer en local sur le port 9000.
[http://localhost:9000](http://localhost:9000)

### Documentation de l'API
La documentation de l'API est accessible à partir de :
[http://localhost:9000/swagger-ui.html#/](http://localhost:9000/swagger-ui.html#/)

## Introduction des données
```bash
mysql -u username -p < data/convention.sql
mysql -u username -p < data/etudiant.sql
```

## Lancement du client
Pour lancer la partie cliente de l'application
```bash
cd client/
mvn clean install
java -jar target/client-0.0.1-SNAPSHOT.jar
```
L'application est lancer en local sur le port 9090.
[http://localhost:9090](http://localhost:9090)
